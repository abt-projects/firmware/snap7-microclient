#ifndef SNAP7_CONFIG_H_
#define SNAP7_CONFIG_H_

// <<< Use Configuration Wizard in Context Menu >>>

// <h> Snap7 Client

//   <o> Buffer Length <512-65536:4>
//   <i> Size of TS7Buffer type in bytes
//   <i> Default: 65536
#define S7CLIENT_BUFFER_SIZE    65536

//   <o> COTP Source Reference <0x0-0xFFFF>
//   <i> Default: 0x0100
#define S7CLIENT_SRC_REF        0x0100

//   <o> COTP Destination Reference <0x0-0xFFFF>
//   <i> Default: 0x0000
#define S7CLIENT_DST_REF        0x0000

//   <o> COTP Source TSAP <0x0-0xFFFF>
//   <i> Default: 0x0100
#define S7CLIENT_SRC_TSAP       0x0100

//   <o> PDU Length <112-960:4>
//   <i> Requested PDU length in bytes
//   <i> Default: 480
#define S7CLIENT_PDU_REQUEST    480

//   <q> SmartConnect
//   <i> Ping the PLC before establishing a TCP connection
#define S7CLIENT_SMARTCONNECT   1

#endif
