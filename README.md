# Snap7 MicroClient for Keil RTX5

Port of [Snap7 MicroClient](http://snap7.sourceforge.net/snap7_source.html#embed_mc) v1.4.2 to [Keil RTX5](https://www.keil.com/pack/doc/CMSIS/RTOS2/html/theory_of_operation.html) real-time operating system and [Network Component](https://www.keil.com/pack/doc/mw/Network/html/index.html) middleware.

## Usage

- A kernel tick frequency of 1000 Hz is assumed (1 ms period).  
  If the kernel is configured differently, [snap_sysutils.cpp](snap_sysutils.cpp) should be adapted to rely on [osKernelGetTickFreq()](https://www.keil.com/pack/doc/CMSIS/RTOS2/html/group__CMSIS__RTOS__KernelCtrl.html).

- The Network Component must have [BSD sockets](https://www.keil.com/pack/doc/mw/Network/html/group__net_b_s_d___func.html) enabled.

- Some default parameters are adjustable in file [snap7_config.h](_snap7_config.h) (the one provided is an example, it should be copied to the project directory and edited):
  - Client Buffer Length (default: 65536 bytes)
    - This buffer is not always tested for overflow; beware not to request data exceeding the buffer size!
  - COTP Source Reference (default: 0x0100)
  - COTP Destination Reference (default: 0x0000)
  - COTP Source TSAP (default: 0x0100)
  - Requested PDU length (default: 480 bytes)
  - SmartConnect enable/disable

## Porting notes

- Since the BSD sockets API returns the detailed error directly without the need to check `errno`, all calls to `GetLastSocketError()` were removed.
- The BSD sockets API doesn't support `FIONREAD` in `ioctl()`. Calls to `WaitForData()` were replaced by a non-blocking call to `recv()` with flags `MSG_DONTWAIT` and `MSG_PEEK` to check if we can peek the required amount of data.
- The BSD sockets API doesn't support `TCP_NODELAY` flag and doesn't provide `shutdown()` function.
- `TPinger` and `TRawSocketPinger` classes were replaced by calls to the native ping client of the Network Component (`netPing_EchoX()`).
- Incompatible functions not used by the MicroClient were simply disabled in source files (calling them will generate a linker error).
- The size of the temporary buffer used to purge the socket was changed from 512 bytes to 32 bytes.

Show the [diff](//gitlab.cern.ch/abt-projects/firmware/snap7-microclient/-/compare/54c0dcb0...master)
